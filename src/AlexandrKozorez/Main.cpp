#include "Manager.h"
#include "Engineer.h"
#include "Personal.h"

vector <Employee*> workers_list;
int worktime, base, qual, id, n;
string name, name1, name2, name3, type, project;
double part;

void initializeEmployee(string path)
{

	ifstream file_cin(path);
	while (file_cin >> id)
	{
		file_cin >> name1 >> name2 >> name3 >> type >> worktime;
		name = name1 + " " + name2 + " " + name3;
		if (type == "Cleaner")
		{
			file_cin >> base;
			workers_list.push_back(new Cleaner(id, name, base, worktime));
		}
		if (type == "Driver") {
			file_cin >> base;
			workers_list.push_back(new Driver(id, name, base, worktime));
		}
		if (type == "Tester")
		{
			file_cin >> base >> project >> part;
			workers_list.push_back(new Tester(id, name, base, project, part, worktime));
		}
		if (type == "Programmer")
		{
			file_cin >> base >> project >> part;
			workers_list.push_back(new Programmer(id, name, base, project, part, worktime));
		}
		if (type == "TeamLeader")
		{
			file_cin >> base >> qual >> project >> part;
			workers_list.push_back(new TeamLeader(id, name, base, project, qual, part, worktime));
		}
		if (type == "Manager")
		{
			file_cin >> project >> part;
			workers_list.push_back(new Manager(id, name, project, part, worktime));
		}
		if (type == "ProjectManager")
		{
			file_cin >> qual >> project >> part;
			workers_list.push_back(new ProjectManager(id, name, project, qual, part, worktime));
		}
		if (type == "SeniorManager")
		{
			file_cin >> qual >> project >> part;
			workers_list.push_back(new SeniorManager(id, name, project, qual, part, worktime));
		}
	}
}

void printIncome(string path)
{
	ofstream file_cout(path);
	for (int i = 0;i < n;i++)
	{
		Employee* e = workers_list[i];
		file_cout << "\nID: " << e->getID() << "\n" 
				  << "Name: " << e->getName() << "\n"
				  << "Payment: " << e->getPayment() << "\n";
	}
}

int main()
{
	initializeEmployee("workers_list.txt");
	n = workers_list.size();
	
	for (int i = 0;i < n;i++) 
		workers_list[i]->calculate();
	
	printIncome("payment.txt");
	return 0;
}