#ifndef TML
#define TML
#include "Programmer.h"
#include "Heading.h"
#include <iostream>
#include <string>
using namespace std;
class TeamLeader : public Programmer, public Heading
{
private:
    int number;
    int stakeFor1;
public:
    TeamLeader() {};
    TeamLeader(string id, string name, int workTime, double payment, string projectName, double part, int base, int budget,int number, int stakeFor1):Programmer(id, name, workTime, payment, projectName, part, base, budget)
    {
        this->number = number;
        this->stakeFor1 = stakeFor1;
    }

    int EmplNumbSalary(int stakeFor1, int number) override
    {
        return stakeFor1 * number;
    }

    void CalcPayment() override
    {
        SetPayment(WorkTimeSalary(GetWorkTime(), base) + ProjectSalary(budget, part)+ EmplNumbSalary(number, stakeFor1));
    }

    void Print_Info() override
    {
        cout << endl<<"Id: " << GetId() <<endl <<"Name: "<< GetName()<< endl << "Profession: Team Leader" << endl
        << "Project Name: " << projectName << endl << "Budget: " << budget << endl << "Part: " << part << endl
        << "Employees Number: " << number << endl << "Stake for one: " << stakeFor1 << endl 
        << "Work Time: " << GetWorkTime() << " hours" << endl<< "Base: " << base << endl << "Salary: " << GetPayment() << endl;
    }
};
#endif

