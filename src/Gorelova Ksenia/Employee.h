#pragma once
#include <string>

using namespace std;

class Employee
{
protected:
	int id;
	string name;
	int wtime;
	double payment;
public:
	Employee() : id(0), name(""), wtime(0), payment(.0) {}
	Employee(int id, string name, int wtime, double payment)
	{
		this->id = id;
		this->name = name;
		this->wtime = wtime;
		this->payment = payment;
	}
	~Employee() {}
	virtual void Calculation() = 0;
	virtual void Info(ostream& str) = 0;
	int getId() { return id; }
	string getName() { return name; }
	int getWT() { return wtime; }
	double getPayment() { return payment; }
};

class WorkTime
{
public:
	virtual int HourlyPay(int wtime, int base) = 0;
};

class Project
{
public:
	virtual double PayForProject(double part, int budget) = 0;
};

class Heading
{
public:
	virtual int PayForPost(int pay, int num) = 0;
};