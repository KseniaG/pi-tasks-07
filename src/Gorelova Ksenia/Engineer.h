#pragma once
#include "Employee.h"

using namespace std;

class Engineer : public Employee, public WorkTime, public Project
{
protected:
	int base;
	double part;
	int budget;
	string project;
public:
	Engineer() : Employee(), part(.0), budget(0), project("") {}
	Engineer(int id, string name, int wtime, double payment, int base, double part, int budget, string project) :
		Employee(id, name, wtime, payment)
	{
		this->base = base;
		this->part = part;
		this->budget = budget;
		this->project = project;
	}
	~Engineer() {}

	double PayForProject(double part, int budget)
	{
		return part*budget;
	}

	int HourlyPay(int wtime, int base)
	{
		return wtime*base;
	}

	void Calculation()
	{
		payment = PayForProject(part, budget) + HourlyPay(wtime, base);
	}
};

class Programmer : public Engineer
{
public:
	Programmer() : Engineer() {}
	~Programmer() {}

	Programmer(int id, string name, int base, string project, double part, int budget, int wtime, double payment) :
		Engineer(id, name, wtime, payment, base, part, budget, project) {}

	void Info(ostream& str)
	{
		str << "ID: " << id << "\nName: " << name << "\nPosition: Programmer \nWorktime: " << wtime;
		str << "\nBase: " << base << "\nProject title: " << project << "\nBudget: " << budget;
		str << "\nInvolvement: " << part << "\nPayment: " << payment << endl << endl;
	}
};

class Tester : public Engineer
{
public:
	Tester() : Engineer() {}
	~Tester() {}

	Tester(int id, string name, int base, string project, double part, int budget, int wtime, double payment) :
		Engineer(id, name, wtime, payment, base, part, budget, project) {}

	void Info(ostream& str)
	{
		str << "ID: " << id << "\nName: " << name << "\nPosition: Tester \nWorktime: " << wtime;
		str << "\nBase: " << base << "\nProject title: " << project << "\nBudget: " << budget;
		str << "\nInvolvement: " << part << "\nPayment: " << payment << endl << endl;
	}
};

class TeamLeader : public Engineer, public Heading
{
protected:
	double pay;
	int num;
public:
	TeamLeader() :Engineer(), pay(.0), num(0) {}
	TeamLeader(int id, string name, int base, string project, double part, int budget, int wtime, double pay, int num, double payment) :
		Engineer(id, name, wtime, payment, base, part, budget, project)
	{
		this->pay = pay;
		this->num = num;
	}
	~TeamLeader() {}

	int PayForPost(int pay, int num)
	{
		return pay * num;
	}

	void Calculation()
	{
		payment = HourlyPay(wtime, base) + PayForProject(part, budget) + PayForPost(pay, num);
	}

	void Info(ostream& str)
	{
		str << "ID: " << id << "\nName: " << name << "\nPosition: Tester \nWorktime: " << wtime;
		str << "\nBase: " << base << "\nProject title: " << project << "\nBudget: " << budget;
		str << "\nInvolvement: " << part << "\nFee for each subordinate: " << pay << "\nNumber of subordinates: " << num;
		str << "\nPayment: " << payment << endl << endl;
	}
};