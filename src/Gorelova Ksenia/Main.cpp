#include "Employee.h"
#include "Engineer.h"
#include "Manager.h"
#include "Personal.h"
#include <iostream>
#include <fstream>
#include <string>

using namespace std;

int main()
{
	int Num = 0;
	int base;
	int wtime;
	int pay, num;
	int numS;
	string project;
	double part;
	int budget;
	setlocale(LC_ALL, "Russian");
	Employee * staff[40];
	ifstream file("Staff.txt");
	if (!file.is_open())
		cout << "Error openning file" << endl;
	else
	{
		int id = 0;
		string position, name, n1, n2, n3;
		while (!file.eof())
		{
			file >> id >> n1 >> n2 >> n3 >> position;
			name = n1 + " " + n2 + " " + n3;

			if (position == "Cleaner")
			{
				file >> base >> wtime;
				staff[Num] = new Cleaner(id, name, base, wtime, 0);
				Num++;
			}

			if (position == "Driver")
			{
				file >> base >> wtime;
				staff[Num] = new Driver(id, name, base, wtime, 0);
				Num++;
			}

			if (position == "Programmer")
			{
				file >> base >> project >> part >> budget >> wtime;
				staff[Num] = new Programmer(id, name, base, project, part, budget, wtime, 0);
				Num++;
			}

			if (position == "Tester")
			{
				file >> base >> project >> part >> budget >> wtime;
				staff[Num] = new Tester(id, name, base, project, part, budget, wtime, 0);
				Num++;
			}

			if (position == "TeamLeader")
			{
				file >> base >> project >> part >> budget >> wtime >> pay >> num;
				staff[Num] = new TeamLeader(id, name, base, project, part, budget, wtime, pay, num, 0);
				Num++;
			}

			if (position == "ProjectManager")
			{
				file >> pay >> project >> part >> budget >> num;
				staff[Num] = new ProjectManager(id, name, 0, pay, project, part, budget, num, 0);
				Num++;
			}

			if (position == "SeniorManager")
			{
				file >> pay >> project >> num >> part >> budget >> numS;
				staff[Num] = new SeniorManager(id, name, 0, pay, project, num, part, budget, numS, 0);
				Num++;
			}
		}
	}
	file.close();

	ofstream newFile("newStaff.txt");
	for (int i = 0; i < Num; i++)
	{
		staff[i]->Calculation();
		staff[i]->Info(newFile);
	}
}