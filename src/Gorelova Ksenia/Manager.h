#pragma once
#include <string>
#include "Employee.h"

using namespace std;

class Manager : public Employee, public Project
{
protected:
	string project;
	int budget;
	double part;
public:
	Manager() : Employee(), project(""), budget(0), part(.0) {}
	Manager(int id, string name, int wtime, double payment, string project, int budget, double part) :
		Employee(id, name, wtime, payment)
	{
		this->project = project;
		this->budget = budget;
		this->part = part;
	}
	~Manager() {}

	double PayForProject(double part, int budget)
	{
		return part*budget;
	}

	void Calculation()
	{
		payment = PayForProject(part, budget);
	}
};

class ProjectManager : public Manager, public Heading
{
protected:
	double pay;
	int num;
public:
	ProjectManager() :Manager(), pay(.0), num(0) {}
	ProjectManager(int id, string name, int wtime, double pay, string project, double part, int budget, int num, double payment) :
		Manager(id, name, wtime, payment, project, budget, part)
	{
		this->pay = pay;
		this->num = num;
	}

	~ProjectManager() {}

	int PayForPost(int pay, int num)
	{
		return pay * num;
	}

	void Calculation()
	{
		payment = PayForProject(part, budget) + PayForPost(pay, num);
	}

	void Info(ostream& str)
	{
		str << "ID: " << id << "\nName: " << name << "\nPosition: Project Manager \nProject title: " << project;
		str << "\nBudget: " << budget << "\nInvolvement: " << part << "\nFee for each subordinate: " << pay;
		str << "\nNumber of subordinates: " << num << "\nPayment: " << payment << endl << endl;
	}
};

class SeniorManager : public ProjectManager, public Project
{
public:
	int numS;
	SeniorManager() :ProjectManager() {}
	SeniorManager(int id, string name, int wtime, double pay, string project, int num, double part, int budget, int numS, double payment) :
		ProjectManager(id, name, wtime, pay, project, part, budget, num, payment)
	{
		this->numS = numS;
	}
	~SeniorManager() {}

	double PayForProject(double part, int budget)
	{
		return (int)(part*budget) * numS;
	}

	void Info(ostream& str)
	{
		str << "ID: " << id << "\nName: " << name << "\nPosition: Senior Manager \nProject title: " << project;
		str << "\nNumber of projects:" << num << "\nBudget: " << budget << "\nInvolvement: " << part;
		str << "\nFee for each subordinate: " << pay << "\nNumber of subordinates: " << numS << "\nPayment: " << payment << endl << endl;
	}
};