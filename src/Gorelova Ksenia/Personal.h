#pragma once
#include <string>
#include "Employee.h"

using namespace std;

class Personal : public Employee, public WorkTime
{
protected:
	int base;
public:
	Personal() : Employee(), base(0) {}

	Personal(int id, string name, int wtime, double payment, int base) :Employee(id, name, wtime, payment)
	{
		this->base = base;
	}

	~Personal() {}

	int HourlyPay(int wtime, int base)
	{
		return wtime*base;
	}

	void Calculation()
	{
		payment = HourlyPay(wtime, base);
	}
};

class Cleaner : public Personal
{
public:
	Cleaner() : Personal() {}
	~Cleaner() {}

	Cleaner(int id, string name, int base, int wtime, double payment) : Personal(id, name, wtime, payment, base) {}

	void Info(ostream& str)
	{
		str << "ID: " << id << "\nName: " << name << "\nPosition: Cleaner \nWorktime: " << wtime;
		str << "\nBase: " << base << "\nPayment: " << payment << endl << endl;
	}
};

class Driver : public Personal
{
public:
	Driver() : Personal() {}
	~Driver() {}

	Driver(int id, string name, int base, int wtime, double payment) : Personal(id, name, wtime, payment, base) {}

	void Info(ostream& str)
	{
		str << "ID: " << id << "\nName: " << name << "\nPosition: Driver \nWorktime: " << wtime;
		str << "\nBase: " << base << "\nPayment: " << payment << endl << endl;
	}
};
