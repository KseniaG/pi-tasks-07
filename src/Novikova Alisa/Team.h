#pragma once
#include "Employee.h"
#include "Interface.h"

class Engineer : public Employee, public Worktime, public Project, public Heading
{
protected:
	int base;
	int budget;
	float ratio;
	string project;
public:
	int PayTime(int workedOut, int base) { return workedOut * base; }

	int PayProject(int budget, double ratio) { return budget * (int)ratio; }

	int PayHeading(int pay, int numberEmp) { return pay * numberEmp; }
};

class Programmer : public Engineer
{
private:
	float ratio = 0.07;
public:
	Programmer(int id, string name, string project, int base)
	{
		this->id = id;
		this->name = name;
		this->project = project;
		this->base = base;
	}

	void Pay()
	{
		payment = PayProject(budget, ratio) + PayTime(base, time);
	}
};

class Tester : public Engineer
{
private:
	float ratio = 0.04;
public:
	Tester(int id, string name, string project, int base)
	{
		this->id = id;
		this->name = name;
		this->project = project;
		this->base = base;
	}

	void Pay()
	{
		payment = PayProject(budget, ratio) + PayTime(base, time);
	}
};

class TeamLeader : public Engineer
{
private:
	float ratio = 0.3;
	int pay = 1500;
	int numberEmp;
public:
	TeamLeader(int id, string name, string project, int base, int budget, int numberEmp)
	{
		this->id = id;
		this->name = name;
		this->project = project;
		this->base = base;
		this->budget = budget;
		this->numberEmp = numberEmp;
	}

	void Pay()
	{
		payment = PayTime(time, base) + PayProject(budget, ratio)
			+ PayHeading(pay, numberEmp);
	}
};